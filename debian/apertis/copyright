Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 1989, 1990, 1992, 1993, The Regents of the University of California.
License: BSD-3-clause

Files: aclocal.m4
Copyright: 1996-2024, Free Software Foundation, Inc.
License: FSFULLR

Files: compile
 depcomp
 missing
Copyright: 1996-2024, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: configure
Copyright: 1992-1996, 1998-2017, 2020-2023, Free Software Foundation
License: FSFUL

Files: debian/*
Copyright: This code is derived from software contributed to Berkeley by
 The Regents of the University of California.
 Christos Zoulas of Cornell University.
 1992, 1993, Copyright
License: BSD-3-clause

Files: doc/*
Copyright: 1997-2014, The NetBSD Foundation, Inc.
License: BSD-2-clause

Files: doc/editline.7.roff
Copyright: 2016, Ingo Schwarze <schwarze@openbsd.org>
License: ISC

Files: doc/mdoc2man.awk
Copyright: 2003, Peter Stuge <stuge-mdoc2man@cdy.org>
License: ISC

Files: install-sh
Copyright: 1994, X Consortium
License: X11

Files: ltmain.sh
Copyright: 1996-2019, 2021-2024, Free Software Foundation, Inc.
License: (Expat or GPL-2+) with Libtool exception

Files: m4/*
Copyright: 2004, 2005, 2007-2009, 2011-2019, 2021-2024, Free
License: FSFULLR

Files: m4/libtool.m4
Copyright: 1996-2001, 2003-2019, 2021-2024, Free Software
License: (FSFULLR or GPL-2+) with Libtool exception

Files: m4/ltsugar.m4
Copyright: 2004, 2005, 2007, 2008, 2011-2019, 2021-2024, Free Software
License: FSFULLR

Files: m4/ltversion.m4
Copyright: 2004, 2011-2019, 2021-2024, Free Software Foundation
License: FSFULLR

Files: src/chartype.c
 src/chartype.h
 src/eln.c
Copyright: 1997-2014, The NetBSD Foundation, Inc.
License: BSD-2-clause

Files: src/editline/*
Copyright: 1997, 2001, 2011, 2017, The NetBSD Foundation, Inc.
License: BSD-2-Clause-NetBSD or BSD-2-clause

Files: src/filecomplete.c
 src/filecomplete.h
 src/getline.c
 src/literal.c
 src/literal.h
 src/read.h
 src/readline.c
Copyright: 1997, 2001, 2011, 2017, The NetBSD Foundation, Inc.
License: BSD-2-Clause-NetBSD or BSD-2-clause

Files: src/reallocarr.c
Copyright: 2015, Joerg Sonnenberger <joerg@NetBSD.org>.
License: BSD-2-clause

Files: src/strlcat.c
 src/strlcpy.c
Copyright: 1998, Todd C. Miller <Todd.Miller@courtesan.com>
License: ISC

Files: src/vis.c
Copyright: 1989, 1993, The Regents of the University of California.
License: BSD-2-clause or BSD-3-clause

Files: INSTALL Makefile.in doc/Makefile.in examples/Makefile.in src/Makefile.in src/wcsdup.c
Copyright:
 Copyright (c) 1992, 1993
 The Regents of the University of California.  All rights reserved.
 .
 This code is derived from software contributed to Berkeley by
 Christos Zoulas of Cornell University.
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.
